export const CssConstant = {
  root: {
    fontFamily: "'Lexend Deca', sans-serif"
  },
  errorText: {
    color: 'red',
    fontSize: 13
  }
}